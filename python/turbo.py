import json

import random

QUOTES = ["Se lahestyy. Maailman lapset voivat valittaa ja pelata, mutta filosofi ei koskaan!",
		"Tama on katastrofi",
		"Filosofit ovat pyrstotahtien ylapuolella",
		"Eihan tassa kuumuudessa voi edes ajatella.",
		"Pikkuleipia tassa tilanteessa? No ehka yksi...",
		"Suuri taide edellyttaa suurta karsimysta.",
		"Filosofit murskaantuvat kuitenkin.",
		"Tuho tulee vaistamatta.",
		"Hyodytonta murehtia koska mitaan ei voi tehda."]

class Turbo(object):
	def __init__(self):
		self.turbo_factor = None
		self.ticks = None
		self.is_usable = False
		self.is_activated = False
		self.activation_tick = None

	def reload(self, ticks, turbo_factor):
		self.ticks = ticks
		self.turbo_factor = turbo_factor
		self.is_usable = True

	def activate(self, activation_tick, message_queue):
		#print("TURBO ACTIVATED!")
		self.is_activated = True
		self.is_usable = False
		self.activation_tick = activation_tick
		message_queue.append(json.dumps({"msgType": "turbo", "data": random.choice(QUOTES)}))

	def update(self, current_tick):
		if self.is_activated:
			if current_tick > self.activation_tick + self.ticks:
				self.is_activated = False