import json
import socket
import sys

import track, cars


class Bot(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        self.track = None
        self.cars = None
        self.color = None
        self.message_queue = []
        self.game_tick = 0

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def queue_msg(self, msg_type, data):
        self.message_queue.append(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def create_race_and_communicate(self, track_name, car_count):
        self.msg("createRace", {"botId": {
            "name": self.name,
            "key": self.key
            },
            "trackName": track_name,
            "carCount": car_count
            })
        self.msg_loop()    

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def communicate(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def on_your_car(self, my_car):
        self.color = my_car["color"]

    def on_car_positions(self, data):
        if self.cars == None:
            self.cars = cars.Cars(data, self.color)
            self.on_piece_changed()
        #update position data and fire piece_change event if necessary
        if self.cars.update(data, self.game_tick):
            self.on_piece_changed()
        #self.throttle(0.65)
        self.on_game_tick(data)

    def on_piece_changed(self):
        current_piece_index = self.cars.my_car()["piecePosition"]["pieceIndex"]
        if self.track.next_turbo_piece_index(current_piece_index) == current_piece_index and self.cars.turbo.is_usable:
            self.cars.turbo.activate(self.game_tick, self.message_queue)
        change_lane = self.track.switch_lane(current_piece_index)
        #print("Change lane: %s" % (change_lane))
        if change_lane != None:
            self.queue_msg("switchLane", change_lane)
        #print("velocity: %s,  angle: %s" % (self.cars.velocity, self.cars.my_car()["angle"]))

    def on_turbo_available(self, turbo_data):
        self.cars.turbo.reload(turbo_data["turboDurationTicks"], turbo_data["turboFactor"])

    def on_crash(self, data):
        print("Someone crashed")
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def on_game_init(self, data):
        print("Got game init data")
        self.track = track.Track(data["race"]["track"])

    def on_game_tick(self, car_positions):        
        if len(self.message_queue) > 0:
            self.send(self.message_queue.pop())
        else:
            self.throttle(self.cars.calculate_throttle(self.track))

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameInit': self.on_game_init,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'yourCar': self.on_your_car,
            'turboAvailable': self.on_turbo_available,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
                if 'gameTick' in msg:
                    self.game_tick = max(self.game_tick, msg['gameTick'])
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()

if __name__ == "__main__":
    if len(sys.argv) == 5:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = Bot(s, name, key)
        bot.communicate()
        print("Usage: ./run host port botname botkey")
    elif len(sys.argv) == 7:
        host, port, name, key, track_name, car_count = sys.argv[1:7]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}, track name={4}, botkey={5}".format(*sys.argv[1:7]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = Bot(s, name, key)
        bot.create_race_and_communicate(track_name, car_count)
    else:
        print("Usage 1: ./run host port botname botkey")
        print "Usage 2: ./run host port botname botkey trackname carcount"
        #print("Usage 3: ./run host port botname botkey trackname password carcount")