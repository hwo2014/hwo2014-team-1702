import math
from itertools import *

class Track(object):
	def __init__(self, track):
		self.track = track

	#calculate if we should switch lane
	def switch_lane(self, current_piece_index):
		current_piece = self.track["pieces"][current_piece_index]
		#print("Piece: %s -- %s" %(current_piece_index, current_piece))
		arc_len = 0.0
		piece_gen = self.cyclic_piece_generator(current_piece_index + 1)
		for piece, index in piece_gen:
			if "switch" in piece:
				break
		for piece, index in piece_gen:
			if "angle" in piece:
				arc_len += math.radians(piece["angle"])*piece["radius"]
			if "switch" in piece:
				break
		#print("Arc sum: %s" % (arc_len))
		if arc_len > 0:
			return "Right"
		elif arc_len < 0:
			return "Left"
		else:
			return None

	def line_len(self, cars):
		line_len = self.piece_line_len(cars.my_car()["piecePosition"]["pieceIndex"])
		in_piece_distance = cars.my_car()["piecePosition"]["inPieceDistance"]
		return max(0.0, line_len - in_piece_distance)

	def next_turn(self, current_piece_index):
		return (piece for piece, index in self.cyclic_piece_generator(current_piece_index) if "angle" in piece).next()


	def piece_line_len(self, current_piece_index):
		line = takewhile(lambda (piece, index): "angle" not in piece, self.cyclic_piece_generator(current_piece_index))
		return reduce(lambda length, (piece, index): length + piece["length"], line, 0.0)

	def next_turbo_piece_index(self, current_piece_index):
		best_index = current_piece_index
		best_len = self.piece_line_len(current_piece_index)
		for piece, i in self.cyclic_piece_generator(current_piece_index + 1):
			if i == current_piece_index:
				break
			piece_line_len = self.piece_line_len(i)
			if piece_line_len > best_len:
				best_index, best_len = (i, piece_line_len)
		return best_index


	#iterate over pieces as if it was a cyclic linked list starting at piece_index
	def cyclic_piece_generator(self, piece_index):
		i = piece_index
		pieces = self.track["pieces"]
		while True:
			if i == len(pieces):
				i = 0
			yield pieces[i], i
			i += 1


