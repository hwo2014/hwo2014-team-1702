import math

import conf, turbo

class Cars(object):
	def __init__(self, car_positions, color):
		self.color = color
		self.car_positions = car_positions
		self.velocity = 0
		self.turbo = turbo.Turbo()

	#return true if my track piece changed
	def update(self, car_positions, game_tick):
		self.turbo.update(game_tick)
		last_car_position = self.my_car()["piecePosition"]
		new_car_position = self._my_car(car_positions)["piecePosition"]
		signal = last_car_position["pieceIndex"] != new_car_position["pieceIndex"]
		if not signal:
			dx = new_car_position["inPieceDistance"] - last_car_position["inPieceDistance"]
			# v = dx/dt where dt = constant
			self.velocity = dx 
		self.car_positions = car_positions
		return signal

	def calculate_throttle(self, track):
		current_piece_index = self.my_car()["piecePosition"]["pieceIndex"]
		current_piece = track.track["pieces"][current_piece_index]
		current_angle = abs(self.my_car()["angle"])

		is_turn = "angle" in current_piece
		next_turn = track.next_turn(current_piece_index)
		next_turning_velocity = self.max_turning_velocity(next_turn, track)
		#next_turning_throttle = self.get_throttle(next_turning_velocity)

		#print("turning velocity: %s,  max throttle: %s" %(next_turning_velocity, self.get_throttle(next_turning_velocity)))
		power = 1.0
		if current_angle > conf.DRIFTING_ANGLES[0]: #not healthy_turn and current_angle > conf.DRIFTING_ANGLES[0]:
			if current_angle > conf.DRIFTING_ANGLES[1]:
				return 0.0
			power = 1.0 - ((current_angle - conf.DRIFTING_ANGLES[0])/(conf.DRIFTING_ANGLES[1] - conf.DRIFTING_ANGLES[0]))
		#print("slowdown coeff: %s, angle %s, danger angle: %s" % (power, current_angle, conf.DRIFTING_ANGLES[0]))

		#line_len = track.line_len(self)
		#on a line
		#if line_len > 0.0:
		#	if line_len > self.calculate_braking_distance(self.velocity, next_turning_velocity):
		#		return 1.0*power
		#	else:
		#		return 0.0
		#on a turn
		#else:
		#	if self.velocity < next_turning_velocity:
		#		return 1.0*power
		#	else:
				#return 0.0
				#return 1.0 - self.velocity/(next_turning_velocity + self.velocity)
		#		return self.get_throttle(next_turning_velocity)*power
		brake_next = self.brake_next(current_piece_index, track)

		if brake_next:
			return 0.0

		if "angle" in current_piece and abs(next_turning_velocity - self.velocity) < 0.2:
			return self.get_throttle(next_turning_velocity) * power
		else:
			return power * 1.0

	#calculate how long line is needed to drop velocity from current velocity to target velocity
	def calculate_braking_distance(self, current_velocity, target_velocity):
		if current_velocity <= target_velocity:
			return 0.0

		length = (current_velocity - target_velocity) / conf.BRAKE_CONSTANT
		#arg = 0.5 * (1 - (target_velocity**2) / (current_velocity**2))
		#length *= math.e ** arg
		#print("Braking distance: %s,  velocity: %s,  angle: %s" % (length, current_velocity, self.my_car()["angle"]))
		return length

	def brake_next(self, current_piece_index, track):
		current_piece = track.track["pieces"][current_piece_index]
		in_piece_distance = self.my_car()["piecePosition"]["inPieceDistance"]
		if "angle" in current_piece:
			max_turning_velocity = self.max_turning_velocity(current_piece, track)
			if max_turning_velocity < self.velocity:
				return True
		distance = -in_piece_distance
		distance += current_piece["length"] if "length" in current_piece else abs(math.radians(current_piece["angle"])*current_piece["radius"])		
		for piece, index in track.cyclic_piece_generator(current_piece_index + 1):			
			if "angle" in piece:
				max_turning_velocity = self.max_turning_velocity(piece, track)
				braking_distance = self.calculate_braking_distance(self.velocity, max_turning_velocity)
				#print("distance: %s, braking distance: %s" % (distance, braking_distance))
				if distance < braking_distance:
					return True
			if distance > 800:
				return False
			distance += piece["length"] if "length" in piece else abs(math.radians(piece["angle"])*piece["radius"])

	#calculates max turnig velocity from next turning piece
	def max_turning_velocity(self, piece, track):
		lane_correction = min(map(lambda lane: lane["distanceFromCenter"], track.track["lanes"]))
		random_correction = 0.5 * ((100.0 - piece["radius"]) / 100.0)
		return math.sqrt(conf.TURNING_VELOCITY_CONSTANT * (piece["radius"] + lane_correction)) + random_correction

	def get_throttle(self, target_velocity):
		turbo_factor = self.turbo.turbo_factor if self.turbo.is_activated else 1.0
		return target_velocity / (10.0 * turbo_factor)

	def my_car(self):
		return self._my_car(self.car_positions)

	def _my_car(self, car_positions):
		for car in car_positions:
			if car["id"]["color"] == self.color:
				return car
		raise Exception("My car not found")